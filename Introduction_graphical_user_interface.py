# -*- coding: utf-8 -*-


# Author : Marion Estoup
# E-mail : marion_110@hotmail.fr
# November 2021

## Graphical user interface

# Install needed library tk
!pip install tk

# Import needed library tkinter
import tkinter as tk

# Create variable programme thanks to Tk() function
# helps to display the root window and manages all the other components of the 
# tkinter application
programme = tk.Tk()

# Create function that will do what we want to do in our application
# Here each time we'll click on the button it will display 'hello'
def dtest():
	print('hello')


# Create variable B thanks to Button() function
# It's a button where it will be written 'click me'    
B = tk.Button(programme, text = 'click me', command = dtest)

# Use of pack() function on our variable B
# make a widget fill the entire frame
B.pack()

# We run the program with mainloop() function
programme.mainloop()

# The application can show up in front of you or you may have to move your spyder window to see it







# Another application

# Import needed library
import tkinter as tk
from tkinter import ttk

# Create variable programme with Tk() function 
programme = tk.Tk()

# Create a title in our programm "Miles en kilometres"
programme.title("Miles en kilometres")

# Create variable miles with StringVar() function
# Miles will be a string
miles = tk.StringVar()

# Create variable kmetres with StringVar() function
# kmetres will be a string
kmetres = tk.StringVar()

# Create function that will do what we want to do in our application
# Here the user will enter a value for miles and will have the result in km 
# So it's a conversion from miles to km
def convertir(*args):
    kmetres.set(float(miles.get()) * 1.609)

# Create variable fenetre with Frame() function  
# Frame() used for grouping and organizing other widgets in a friendly way  
fenetre = ttk.Frame(programme, padding ="3 3 12 12")

# Apply grid() function on fenetre
# grid() indicates the row and column positioning in its parameter list
fenetre.grid(column=0, row=0)

# Define variable Entree with Entry() function
# provides the single line text-box to the user to accept a value from the user. 
# We can use the Entry widget to accept the text strings from the user for example
Entree = ttk.Entry(fenetre, width=7, textvariable = miles)

# Define Texte1 variable with Label() function
# specifies the container box where we can place the text or images.
# Here it's the string "miles" that will be displayed
Texte1 = ttk.Label(fenetre, text="miles")

# Define location of Entree in the app with grid() function
Entree.grid(column = 2, row = 1)

# Define location of Texte1 in the app with grid() function
Texte1.grid(column = 3, row = 1)

# Use of Label to write a few things on the app : equivaut a (= equals to) and kilometres
ttk.Label(fenetre, text = "equivaut a").grid(column = 1, row = 2)
ttk.Label(fenetre, textvariable = kmetres).grid(column = 2, row = 2)
ttk.Label(fenetre, text = "kilometres").grid(column = 3, row = 2)

# Create the button of the app at a precise location
ttk.Button(fenetre, text="(convertir)", command = convertir).grid(column = 3, row = 3)

# Use of bind() function to bind events
programme.bind("<Return>", convertir)

# Run the application
programme.mainloop()




















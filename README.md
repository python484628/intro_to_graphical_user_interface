# Introduction_graphical_user_interface



In the python file provided you will find a code with two basic application to get to know how graphical user interface can work with tk library :



- The first application will display a button where it's written 'click me' and it will print hello everytime you click on the button



- The second application will display an application to convert miles to km. The user (you) can enter the number of miles that he wants and it will give him the result in km.


Author : Marion Estoup

E-mail : marion_110@hotmail.fr

November 2021
